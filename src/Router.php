<?php
require_once("model/MusicStorage.php");
require_once("view/MainView.php");
require_once("ctl/Controller.php");

//Cette classe permet de gérer les URL

class Router {
    ///protected $connectDB;
  public function __construct(ConnectDB $connectDB){
     $this->connectDB = $connectDB;
  }

  public function main(){
    //session_start();

    //création de la session feedback
    $feedback = key_exists('feedback',$_SESSION) ? $_SESSION['feedback'] : '';

    //On initialise la variable de session feedback à vide
    $_SESSION['feedback'] = '';

    $view = new MainView($this);
    $ctl = new Controller($view, $this->connectDB);

    //création d'une variable GET dont la clé est action
    $action = key_exists('action', $_GET) ? $_GET['action'] : null;

    //création d'une variable GET dont la clé est chant
    $idChanson = key_exists('chant', $_GET)? $_GET['chant']: null;

    if ($action === null) {
      /* Pas d'action demandée : par défaut on affiche
       * la page d'accueil, sauf si une chanson est demandée,
       * auquel cas on affiche sa page. */
      $action = ($idChanson === null)? "Accueil": "voir";
    }


    try {
        switch ($action) {
        case "Accueil":
             $ctl->allChansons();
             break;
        case "Inscription":
             $ctl->newUser();
             break;
        case "Connexion":
             $ctl->connect();
             break;
        case "Contact":
             $view->pageContact();
             break;
        case "ValiderInsc":
            $ctl->inscription($_POST);
            break;
        case "ValiderCon":
            $ctl->connexion($_POST);
            break;
        case "Chansons":
            $ctl->mesChansons();
            break;
        case "Ajouter":
            $ctl->ajouterChanson();
             break;
        case "Deconnexion":
            $ctl->deconnexion();
            break;
        case "ValiderChanson":
            $ctl->saveChanson($_POST);
            break;
        case "voir":
         if ($idChanson === null) {
            $view->makeUnknownActionPage();
          } else {
            $ctl->musicPage($idChanson);
          }
          break;
        case "modifier":
          if ($idChanson === null) {
            $view->makeUnknownActionPage();
          } else {
            $ctl->modifyChanson($idChanson);
          }
        break;
        case "sauverModifs":
          if ($idChanson === null) {
            $view->makeUnknownActionPage();
          } else {
            $ctl->saveChansonModifications($idChanson, $_POST);
          }
          break;
        case "supprimer":
          if ($idChanson === null) {
            $view->makeUnknownActionPage();
          } else {
            $ctl->deleteChant($idChanson);
          }
          break;
        case "confirmerSuppression":
        if ($idChanson === null) {
          $view->makeUnknownActionPage();
        } else {
          $ctl->confirmerSuppression($idChanson);
        }
        break;
      }
 } catch (Exception $e) {}

   $view->render();
}
  /*URL Page d'accueil*/
  public function makeHomePage(){
    return ".?action=Accueil";
  }

  public function contactPage(){
     return ".?action=Contact";
   }

  public function inscriptionPage(){
    return ".?action=Inscription";
  }

  public function connexionPage(){
    return ".?action=Connexion";
  }

  public function validationInscription(){
    return ".?action=ValiderInsc";
  }

 public function urlPageInscrisptionValide(){
    return ".?action=Succes";
  }

  public function validationConnexion(){
    return ".?action=ValiderCon";
  }

  public function validationAjoutChanson(){
      return ".?action=ValiderChanson";
  }

  public function getUserChansons(){
    return ".?action=Chansons";
  }

  public function ajouterChanson(){
    return ".?action=Ajouter";
  }

  public function deconnexionPage(){
    return ".?action=Deconnexion";
  }

  /* URL de la page de la chanson d'identifiant $id */
  public function chansonPage($id) {
    return ".?chant=$id";
  }

  public function chansonModifPage($id){
    return ".?chant=$id&amp;action=modifier";
  }

  public function chansonSuppressionPage($id){
    return ".?chant=$id&amp;action=supprimer";
  }

  public function updateModifiedChanson($id) {
    return ".?chant=$id&amp;action=sauverModifs";
  }

  public function confirmChantDeletion($id){
    return ".?chant=$id&amp;action=confirmerSuppression";
  }
  
}





?>

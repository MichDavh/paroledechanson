<?php
require_once("Router.php");
require_once("model/UserBuilder.php");
require_once("model/MusicBuilder.php");
require_once("model/Music.php");

class MainView {

  protected $router;
  protected $title;
  protected $content;


  public function __construct(Router $router){
    $this->router = $router;
    $this->title = null;
    $this->style = "";
    $this->content = null;
  }

  public function makeHomePage($allchansons){
    $this->title = "TOUTES LES CHANSONS : ";
     $s = '';
     $s.='<hr/>';
    $res = '<div class="liste"><ul>';
    foreach ($allchansons as $music) {
      $imgUri = "C:/wamp64/www/projetChanson/src/upload";
      

      $s .= '<li>';
      $s .= '<a href="'.$this->router->chansonPage($music['id_chanson']).'">';
      $s .= '<img src="'.$imgUri.self::htmlesc($music['photo']).'" alt="'.self::htmlesc($music['artiste']).'"  />';
      $s .= '<div><h5>'.self::htmlesc($music['artiste']).'</h5>';
      $s .= '<h5 style="display:inline;">'.self::htmlesc($music['titre']).'</h5></div>';
      $s .= '</a></li>';
      
    }
    $res .= $s.'</ul></div>';
    $this->content = $res;
  }

  public function pageInscription(UserBuilder $builder){
        $nomRef = $builder->getNomRef();
        $s = "";
        $s .= '<form action='.$this->router->validationInscription().' method="POST">';

        $s .='<h3 class="inscriptTitre">Inscrivez-vous</h3><hr/>';
        $s .= '<p><input type = "text" name = "'.$nomRef.'" placeholder="Nom" value="';
        $s .= self::htmlesc($builder->getData($nomRef));
        $s .="\" />";
        $err = $builder->getErrors($nomRef);
        if ($err !== null)
          $s .= '<span style ="display:inline-block; color:red;">'.$err.'</span>';
        $s .="</p>\n";

        $prenomRef = $builder->getPrenomRef();
        $s .= '<p><input type = "text" name = "'.$prenomRef.'" placeholder="prenom" value="';
        $s .= self::htmlesc($builder->getData($prenomRef));
        $s .="\" />";
        $err = $builder->getErrors($prenomRef);
        if ($err !== null)
          $s .= '<span style ="display:inline-block; color:red;">'.$err.'</span>';
        $s .="</p>\n";


        $loginRef = $builder->getLoginRef();
        $s .= '<p><input type = "text" name = "'.$loginRef.'" placeholder="login" value="';
        $s .= self::htmlesc($builder->getData($loginRef));
        $s .="\"/>";
        $err = $builder->getErrors($loginRef);
        if ($err !== null)
          $s .= '<span style =" display:inline-block; color:red;">'.$err.'</span>';
        $s .="</p>\n";

        $passRef = $builder->getPasswordRef();
        $s .= '<p><input type = "password" name = "'.$passRef.'" placeholder="password" value="';
        $s .= self::htmlesc($builder->getData($passRef));
        $s .="\"/>";
        $err = $builder->getErrors($passRef);
        if ($err !== null)
          $s .= '<span style ="display:inline-block; color:red;">'.$err.'</span>';
        $s .="</p>\n";

        $mailRef = $builder->getMailRef();
        $s .= '<p><input type = "text" name = "'.$mailRef.'" placeholder="Adresse mail" value="';
        $s .= self::htmlesc($builder->getData($mailRef));
        $s .="\"/>";
        $err = $builder->getErrors($mailRef);
        if ($err !== null)
          $s .= '<span style ="display:inline-block; color:red;">'.$err.'</span>';
        $s .="</p>\n";

        $s .= "\n";
        $s .= "<p><input class='valide' type='submit' value='inscription'>\n</p>";

        $s .= '</form>'."\n";
        $s.='<br/><br/><br/><br/>';
        $this->content = $s;
    }

    public function pageConnexion(UserBuilder $builder){
      $s = "";
      $s .='<div class="formulaire">';
     
      $s .= '<form action='.$this->router->validationConnexion().' method="post">';
      $s .='<h3 class="connectTitre">Connectez-vous</h3><hr/>';
      $loginRef = $builder->getLoginRef();
      $s .= '<p><input type = "text" name = "'.$loginRef.'" placeholder="login" value="';
      $s .= self::htmlesc($builder->getData($loginRef));
      $s .="\"/>";
      $err = $builder->getErrors($loginRef);
      if ($err !== null)
        $s .= '<span style ="display:inline-block; color:red;">'.$err.'</span>';
      $s .="</p>\n";

      $passRef = $builder->getPasswordRef();
      $s .= '<p><input type = "password" name = "'.$passRef.'" placeholder="password" value="';
      $s .= self::htmlesc($builder->getData($passRef));
      $s .="\"/>";
      $err = $builder->getErrors($passRef);
      if ($err !== null)
        $s .= '<span style ="display:inline-block;color:red;">'.$err.'</span>';
      $s .="</p>\n";

      $s .= "<p><input class='valide' type='submit' value='connexion'></p>";
      $s .="<hr/>";
      $s.="<span style='display:inline; font-size:10px;'>Vous n'êtes pas encore inscrit ?<a style='color:blue; text-decoration:none; font-size:12px;' href='".$this->router->inscriptionPage()."'><i>  > Inscrivez-vous !!!</i></a></span>";
      $s .= '</form>';
      $s.='</div>';
       $s.='<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>';
      $this->content = $s;
  }

  function pageAjoutChanson(MusicBuilder $builder){
    $this->title = "Nouvelle Chanson";
    $s = "";
    $s .= '<form enctype="multipart/form-data" action='.$this->router->validationAjoutChanson().' method="POST">';
    $ArtisteRef = $builder->getArtisteRef();
    $s .= '<p><input type = "text" name = "'.$ArtisteRef.'" placeholder="Artiste" value="';
    $s .= self::htmlesc($builder->getData($ArtisteRef));
    $s .="\"/>";
    $err = $builder->getErrors($ArtisteRef);
    if ($err !== null)
      $s .= '<span style ="display:inline-block; color:red;">'.$err.'</span>';
    $s .="</p>\n";

    $titreRef = $builder->getTitreRef();
    $s .= '<p><input type = "text" name = "'.$titreRef.'" placeholder="Titre" value="';
    $s .= self::htmlesc($builder->getData($titreRef));
    $s .="\"/>";
    $err = $builder->getErrors($titreRef);
    if ($err !== null)
      $s .= '<span style ="display:inline-block; color:red;">'.$err.'</span>';
    $s .="</p>\n";

    $paroleRef = $builder->getParoleRef();
    $s .= '<p><textarea type = "text" name = "'.$paroleRef.'" cols="" rows="10" placeholder="Paroles...." value="';
    $s .= self::htmlesc($builder->getData($paroleRef));
    $s .="\"/></textarea>";
    $err = $builder->getErrors($paroleRef);
    if ($err !== null)
      $s .= '<span style = "display:inline-block; color:red;">'.$err.'</span>';
    $s .="</p>\n";

    $photoRef = $builder->getPhotoRef();
    $s .= "<input type='file' name='photo' >";
    $s .= self::htmlesc($builder->getData($photoRef));
    $err = $builder->getErrors($photoRef);
    if ($err !== null)
      $s .= '<span style = "display:inline-block; color:red;">'.$err.'</span>';
    $s .="</p>\n";

    $s .= "<input class='valide' type='submit' value='Ajouter'>";
    $s .= "<input class='valide' type='reset' value='Annuler'>";
    $s .= '</form>';
    $this->content = $s;
  }


  public function makeUnknownActionPage() {
    $this->title = "Erreur";
    $this->content = "La page demandée n'existe pas.";
  }

  public function toutesMusic($tableau){
    $s = '';
    foreach ($tableau as $music) {
      $s .= '<li><img src="'.self::htmlesc($music['photo']).'" alt="'.self::htmlesc($music['artiste']).'" style = "width:10px;height:10px;" /></li>';
    }
    $this->content = $s;
  }


  public function mesChansons($tableau){
    $this->title="Voici mes chansons ";
    $s = '';
    $s .='<hr />';
    $res = '<div class="liste"><ul>';
    foreach ($tableau as $music) {
     $imgUri = "C:/wamp64/www/projetChanson/src/upload";


      $s .= '<li>';
      $s .= '<a href="'.$this->router->chansonPage($music['id_chanson']).'">';
      $s .= '<img src="'.$imgUri.self::htmlesc($music['photo']).'" alt="'.self::htmlesc($music['artiste']).'"  />';
      $s .= '<div><h5>'.self::htmlesc($music['artiste']).'</h5>';
      $s .= '<h5 style="display:inline;">'.self::htmlesc($music['titre']).'</h5></div>';
      $s .= '</a></li>';
      
    }
    $res .= $s.'</ul></div>';

    $this->content = $res;
}

public function makeChansonModifPage($id, MusicBuilder $builder) {
    $this->title = "Modifier la chanson";

    $s='';
    $s .= '<form enctype="multipart/form-data" action='.$this->router->updateModifiedChanson($id).' method="POST">';
    $ArtisteRef = $builder->getArtisteRef();
    $s .= '<p><input type = "text" name = "'.$ArtisteRef.'" placeholder="Artiste" value="'.self::htmlesc($builder->getData($ArtisteRef)).'">';
    $err = $builder->getErrors($ArtisteRef);
    if ($err !== null)
      $s .= '<span style ="display:inline-block; color:red;">'.$err.'</span>';
    $s .="</p>\n";

    $titreRef = $builder->getTitreRef();
    $s .= '<p><input type = "text" name = "'.$titreRef.'" placeholder="Titre" value="'.self::htmlesc($builder->getData($titreRef)).'">';
    $err = $builder->getErrors($titreRef);
    if ($err !== null)
      $s .= '<span style ="display:inline-block; color:red;">'.$err.'</span>';
    $s .="</p>\n";

    $paroleRef = $builder->getParoleRef();
    $s .= '<p><textarea type = "text" name = "'.$paroleRef.'" cols="52.5" rows="25" placeholder="Paroles...." value="">';
    $s.=$builder->getData($paroleRef);
    $s .="</textarea>";
    $err = $builder->getErrors($paroleRef);
    if ($err !== null)
      $s .= '<span style = "display:inline-block; color:red;">'.$err.'</span>';
    $s .="</p>\n";

    $photoRef = $builder->getPhotoRef();
    $imgInit = self::htmlesc($builder->getData($photoRef));
    $newImg = key_exists($photoRef, $_FILES) ? $_FILES[$photoRef]['name'] : null;
    if ($newImg===null){
      $s .= '<p><input type="file" name="'.$photoRef.'" value="'.self::htmlesc($builder->getData($photoRef)).'" />';
    }else{
      $s .= '<p><input type="file" name="'.$photoRef.'" value="'.$newImg.'" />';
    }
    
    $err = $builder->getErrors($photoRef);
    if ($err !== null)
      $s .= '<span style = "display:inline-block; color:red;">'.$err.'</span>';
    $s .="</p>\n";

    $s .= "<input class='valide' type='submit' value='Modifier'>";
    $s .= '</form>';
   
    $this->content = $s;
  }

  public function makeChansonPage($music) {
    foreach ($music as $m) {
      $artiste=self::htmlesc($m['artiste']);
      $titre=self::htmlesc($m['titre']);
      $parole=self::htmlesc($m['parole']);
      $photo=self::htmlesc($m['photo']);
    }
  $imgUri = "C:/wamp64/www/projetChanson/src/upload";;
    
  $s = "";
  $s .="<div  class='chant'>";
  $s .='<div ><img src="'.$imgUri.self::htmlesc($photo).'" alt="image"  /></div>';
  $s .="<div ><h5><span>Artiste</span> : ".$artiste."</h5>";
  $s .="<h5><span> Titre</span> : ".$titre."</h5></div>";
  $s .="<h5><span>Parole</span> :</h5><p> ".$parole."</p>";
  $s.='</div>';
  $this->content = $s;
  }


  public function makeChansonPageUser($id, $music) {
    foreach ($music as $m) {
      $artiste=self::htmlesc($m['artiste']);
      $titre=self::htmlesc($m['titre']);
      $parole=self::htmlesc($m['parole']);
      $photo=self::htmlesc($m['photo']);
    }
   
     $imgUri = "C:/wamp64/www/projetChanson/src/upload";;
    
    
    $s = "";
   $s .="<div  class='chant'>";
    $s .='<div ><img src="'.$imgUri.self::htmlesc($photo).'" alt="image"  /></div>';
   $s .="<div ><h5><span>Artiste</span> : ".$artiste."</h5>";
    $s .="<h5><span> Titre</span> : ".$titre."</h5></div>";
   $s .="<h5><span>Parole</span> :</h5><p> ".$parole."</p>";
   
  $s.='<br/>';
    $s .= "<ul >";
   $s .= '<li ><a href="'.$this->router->chansonModifPage($id).'">Modifier</a></li>'."\n";
    $s .= '<li><a href="'.$this->router->chansonSuppressionPage($id).'">Supprimer</a></li>'."\n";
    $s .= "</ul>";
    $s.='</div>';
    $this->content = $s;
  }

  public function makeChansonDeletionPage($id, Music $m) {
    $titre = self::htmlesc($m->getTitre());
    $artiste = self::htmlesc($m->getArtiste());

		$this->title = "Suppression de la chanson ".$titre." de l'artiste ".$artiste;
		$this->content = "<p>La chanson « {$titre} » va être supprimée.</p>\n";
		$this->content .= '<form action="'.$this->router->confirmChantDeletion($id).'" method="POST">'."\n";
		$this->content .= "<button>Confirmer</button>\n</form>\n";
  }
  
  public function makeConfirmDeletedPage(){
    $this->title = "Suppression effectuée";
    $this->content = "<p>La chanson a été correctement supprimée</p>\n";
  }


 function  pageErreurInscription(){
   $this->title = "Erreur d'inscription ";
   $this->content  = "Votre inscription a échoué vous devez renseigner tous les champs du formulaire...!";
 }

  public function pageContact(){
    $this->title = "A propos !";
    $s = '<p style="font-style:italic; font-weight:bold;"> ';

    $s .='Ce projet je l\'ai réalisé dans le cadre  de l\'appronfondissement personnel de mes compétences en PHP Objet et PHP 7 ';
    $s.="Un utilisateur donné peut créer un compte, se connecter, ajouter, les chansons. Il ne peut modifier ";
    $s .=", supprimer que des chansons qu'il a ajouté.";
     $s.='<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>';
    $this->content = $s;
  }

  public static function htmlesc($str){
    return htmlspecialchars($str, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5, 'UTF-8');
  }

  public function makeUnexpectedErrorPage(Exception $e = null){
    $this->title = "Erreur";
    $this->content = "Une erreur inattendue s'est produite";
  }


  protected function getMenuAccueil(){
    return array(
      "Accueil" => $this->router->makeHomePage(),
      "Inscription" => $this->router->inscriptionPage(),
      "Connexion" => $this->router->connexionPage(),
      "Contact" => $this->router->contactPage(),
    );
  }

  public function getMenuUser(){
    return array(
      "Chansons" => $this->router->getUserChansons(),
      "Nouvelle chanson" => $this->router->ajouterChanson(),
      "Deconnexion" => $this->router->deconnexionPage(),
    );
  }

  public function render(){
   
    $title=$this->title;
    $content=$this->content;
    include ("accueil.php");
  }




}






?>

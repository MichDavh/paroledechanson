<?php
session_start();

/**  controleur du site des mots **/
require_once("view/MainView.php");
//require_once("view/ViewUser.php");
require_once("model/ConnectDB.php");
require_once("model/User.php");
require_once("model/UserBuilder.php");
require_once("model/MusicBuilder.php");
require_once("model/Music.php");
require_once("view/MainView.php");






class Controller
{
    protected $vue;
    protected $connectDB;
    protected $currentMusicBuilder;
	  protected $modifiedMusicBuilders;

    public function __construct(MainView $vue, ConnectDB $connectDB)
    {
        $this->vue = $vue;
        $this->connectDB = $connectDB;
        $this->currentColorBuilder = key_exists('currentColorBuilder', $_SESSION) ? $_SESSION['currentColorBuilder'] : null;
		    $this->modifiedColorBuilders = key_exists('modifiedColorBuilders', $_SESSION) ? $_SESSION['modifiedColorBuilders'] : array();
    }

    public function musicPage($id) {
    /* Une musique est demandée, on la récupère en BD */
    $music=array();
    $music = $this->connectDB->read($id);
   // var_dump($music);
   // $m = new Music($music);
    //$data = $m->getData();
    if ( $music === null) {
      /* La musique n'existe pas en BD */
      $this->$vue->makeUnknownActionPage();
    } else {
      /* La musique existe, on prépare la page */
       if (!key_exists('user', $_SESSION)){
          $this->vue->makeChansonPage($music);
        }else{
           $this->vue->makeChansonPageUser($id,$music);
        }
    }
  }

  public function saveChanson(array $data){

    $mB = new MusicBuilder($data);
    if($mB->isValidMusic()){
         $m = new Music($data);
         $data = $m->getData();
         $id = $this->connectDB->createMusic($data);
        
      }else{
           $this->vue->pageAjoutChanson($mB);
      }
  }

  public function inscription(array $data){
      $uB = new UserBuilder($data);
      if($uB->isValidUser()){
        $u = new User($data);
        $this->connectDB->createUser($u);
       
      }else{
        $this->vue->pageInscription($uB);
      }
    }

    public function connexion(array $data){
        $data['nom']=null;
        $data['prenom']=null;
        $data['mail']=null;
        $uB = new UserBuilder($data);
        $u = new User($data);
        $donnees = $u->getData($data);
        $login = $donnees["login"];
        $mdp = $donnees["password"];
        if($uB->isValidConnect()){
          $responses =  $this->connectDB->connexion($u);
          foreach ($responses as $count) {
             $bdLogin = $count['login'];
             $hash = $count['password'];
             if ($login === $bdLogin && password_verify($mdp, $hash)) {
                $_SESSION['user'] = $count;
                break;
            }
          }
        die;
      }else{
        $this->vue->pageConnexion($uB);
      }
    }

    public function allChansons(){
      $toutechanson = $this->connectDB->readAllMusic();
      if (!key_exists('user', $_SESSION)){
       $this->vue->makeHomePage($toutechanson);
     }else{
      $this->meschansons();
     }
    }

    public function mesChansons(){
      $meschansons = $this->connectDB->readMyMusic();
       $this->vue->mesChansons($meschansons);
    }

    public function deconnexion(){
          session_destroy();
          unset($_SESSION['user']);
          die;
    }

    public function newUser(){
      $uB = new UserBuilder();
      $this->vue->pageInscription($uB);

    }

    public function connect(){
      $uB = new UserBuilder();
      $this->vue->pageConnexion($uB);
    }

    public function ajouterChanson(){
         $mB = new MusicBuilder();
         $this->vue->pageAjoutChanson($mB);
    }

    public function modifyChanson($id) {
    /* On récupère en BD la chanson à modifier */
    $chanson = $this->connectDB->read($id);
     $m = new Music( $chanson);
     //$a=$m->getArtiste();
     //echo $a;
     //var_dump($m);
    if ($chanson === null) {
      $this->vue->makeUnknownActionPage();
    } else {
      /* Extraction des données modifiables */
      $cf = MusicBuilder::buildFromMusic($m);
      $mB = new MusicBuilder($cf);
     // var_dump($cf);
      /* Préparation de la page de formulaire */
      $this->vue->makeChansonModifPage($id, $mB);
    }
  }

  public function saveChansonModifications($id, array $data) {
    /* On récupère en BD la chanson à modifier */
    $chanson = $this->connectDB->read($id);
    var_dump($data);
    var_dump($_FILES['photo']['name']);
    $m = new Music( $chanson);
    if ($chanson === null) {
      /* La chanson n'existe pas en BD */
      $this->vue->makeUnknownActionPage();
    } else {
      $cf = new MusicBuilder($data);
      /* Validation des données */
      if ($cf->isValidMusic()) {
        /* Modification de la chanson */
       //$donne= $cf->updateChanson($m);
       //var_dump($donne);
       $ok = $this->connectDB->update($id, $data);
       //echo $data['titre'];
       var_dump($ok);
      if (!$ok)
          throw new Exception("Identifier has disappeared?!");
        /* Préparation de la page de la chanson */
       
        //$this->vue->makeChansonPageUser($id, $chanson);
      } else {
        $mB = new MusicBuilder($data);
        $this->vue->makeChansonModifPage($id, $mB);
      }
    }
  }

  public function deleteChant($id){
    /* On récupère la couleur en BD */
    $chant = $this->connectDB->read($id);
    $m = new Music($chant);
		if ($chant === null) {
			/* La couleur n'existe pas en BD */
			$this->vue->makeUnknownActionPage();
		} else {
			/* La couleur existe, on prépare la page */
			$this->vue->makeChansonDeletionPage($id,  $m);
		}
  }

  public function confirmerSuppression($id){
    /* On récupère la couleur en BD */
    $chant = $this->connectDB->read($id);
    if ($chant === null) {
			/* La couleur n'existe pas en BD */
			$this->vue->makeUnknownActionPage();
		} else {
			/* La couleur existe, on prépare la page */
      $sup=$this->connectDB->delete($id);
      if (!$sup) {
        /* La couleur n'existe pas en BD */
        $this->vue->makeUnknownActionPage();
      } else {
        /* Tout s'est bien passé */
        $this->vue->makeConfirmDeletedPage();
      }
		}
  }


}

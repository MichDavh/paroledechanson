<?php

class Music
{

    private $idchanson;
    private $artiste;
    private $titre;
    private $photo;
    private $parole;
    private $iduser;
    private $data;


    public function __construct($data=null) {
  		if ($data === null) {
  			$data = array(
  				"artiste" => "",
          "titre" => "",
          "photo" => "",
          "parole" => "",
          "iduser" => "",
  		 );
  		}
  		$this->data = $data;
      //$this->setArtiste($data);
      $this->setDonnee($this->data);
  }

    public function getId()
    {
        return $this->idchanson;
    }
    public function getArtiste()
    {
        return $this->artiste;
    }

    public function getTitre()
    {
        return $this->titre;
    }

    public function getParole()
    {
        return $this->parole;
    }

     public function getPhoto()
    {
        return $this->photo;
    }

    public function getUserId()
    {
        return $this->iduser;
    }

    public function getData()
    {
        return $this->data;
    }

   
    public function setArtiste($data)
    {
     // foreach ($data as $m) {
        $this->artiste=$data['artiste'];
      //}
    }

    public function setTitre($data)
    {
        $this->titre=$data['titre'];
    }

    public function setParole($data)
    {
        $this->parole=$data['parole'];
    }

     public function setPhoto($data)
    {
        $this->photo=$data['photo'];
    }

     public function setDonnee($data){
       foreach ($data as $m) {
      $this->setArtiste($m);
      $this->setTitre($m);
      $this->setParole($m);
       $this->setPhoto($m);
     }
    }


}

?>

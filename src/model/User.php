<?php

class User
{

    private $id=null;
    private $nom=null;
    private $prenom=null;
    private $login=null;
    private $mdp=null;
    private $mail=null;
    private $data=null;


    public function __construct($data=null) {
  		if ($data === null) {
  			$data = array(
  				"nom" => "",
          "prenom" => "",
          "login" => "",
          "password" => "",
        );
  		}
  		$this->data = $data;
      $this->setDonnee($this->data);
  }

    public function getId()
    {
        return $this->id;
    }
    public function getNom()
    {
        return $this->nom;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getMpd()
    {
        return $this->mdp;
    }
    public function getMail()
    {
        return $this->mail;
    }

    public function getData(array $don){
      $res = array();
      $res["login"] = $don["login"];
      $res["password"] = $don["pass"];
    return $res;
}

    public function setDonnee($data){
      $this->setNom($data);
      $this->setPrenom($data);
      $this->setLogin($data);
      $this->setMpd($data);
      $this->setMail($data);
    }



    public function setNom($data)
    {
        $this->nom=$data['nom'];
    }

    public function setPrenom($data)
    {
        $this->prenom=$data['prenom'];
    }

    public function setLogin($data)
    {
        $this->login=$data['login'];
    }

    public function setMpd($data)
    {
        $this->mdp=$data['pass'];
    }
    public function setMail($data)
    {
      $this->mail=$data['mail'];
    }

}

?>

<?php

require_once("model/Music.php");
require_once("model/FileBuilder.php");


class MusicBuilder {

	protected $data;
	protected $errors;

	public function __construct($data=null) {
		if ($data === null) {
			$data = array(
        "artiste" => "",
        "titre" => "",
		"photo" => "",
        "parole" => "",
        "iduser" => "",
			);
		}
		$this->data = $data;
		$this->errors = array();
	}

	public static function buildFromMusic(Music $m){
		$artiste=$m->getArtiste();
		$titre=$m->getTitre();
		$parole=$m->getParole();
		$photo=$m->getPhoto();
		return array(
         "artiste" => $artiste,
         "titre" => $titre,
         "photo"=> $photo,
         "parole" => $parole,
		);
	}


	/* Vérifie la validité des données envoyées par le client,
	 * et renvoie un tableau des erreurs à corriger. */
	public function isValidMusic() {
		$this->errors = array();
    $mailRegex = "#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#";
		if (!key_exists("artiste", $this->data) || $this->data["artiste"] === "")
			$this->errors["artiste"] = "Vous devez entrer un nom d'artiste";
	  elseif (mb_strlen($this->data["artiste"], 'UTF-8') <= 3)
			$this->errors["artiste"] = "Le nom doit faire plus de 3 caractères";
	  if (!key_exists("titre", $this->data) || $this->data["titre"] === "")
			$this->errors["titre"] = "Vous devez entrer un titre";
	//elseif (!preg_match("/^[A-Za-z]*$/i", $this->data["titre"]))
	//		$this->errors["titre"] = "Caractères autorisés : A....Z et a....z";
		if (!key_exists("parole", $this->data) || $this->data["parole"] === "")
			$this->errors["parole"] = "Vous devez mettre les paroles de chanson";
		//elseif (mb_strlen($this->data["parole"], 'UTF-8') <= 3)
		//	$this->errors["parole"] = "Vous devez entrer au minimum 5 caractères ";
		return count($this->errors) === 0;
	}

  public function getErrors($ref){
    return key_exists($ref,$this->errors)? $this->errors[$ref]:null;
  }

  public function getData($ref){
    return key_exists($ref, $this->data) ? $this->data[$ref] : '';
  }

  public function getArtisteRef(){
    return "artiste";
  }

  public function getTitreRef(){
    return "titre";
  }

	public function getPhotoRef(){
		return "photo";
	}

  public function getParoleRef(){
    return "parole";
  }

  public function getIdUserRef(){
    return "iduser";
  }

  public function updateChanson(Music $m) {
		if (key_exists("artiste", $this->data))
			$m->setArtiste($this->data);
		if (key_exists("titre", $this->data))
			$m->setTitre($this->data);
		if (key_exists("parole", $this->data))
			$m->setParole($this->data);
		if (key_exists("photo", $this->data))
			$m->setPhoto($this->data);

		return $this->data;

	}



}

?>

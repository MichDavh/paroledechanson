<?php

require_once("model/User.php");

class UserBuilder {

	protected $data;
	protected $errors;

	public function __construct($data=null) {
		if ($data === null) {
			$data = array("nom" => "", "prenom" => "", "login" => "", "password" => "", "mail" => "",);
		}
		$this->data = $data;
		$this->errors = array();
	}

	public static function buidFromUser(User $u){
		return array("login" => $u->getLogin(), "password" => $u->getMpd(),);
	}

	//Cette fonction permet de vérifier les champs du formulaire
	//de création d'un utilisateur
	public function isValidUser() {
		$this->errors = array();
    	$mailRegex = "#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#";

		if (!key_exists("nom", $this->data) || $this->data["nom"] === "")
			$this->errors["nom"] = "Vous devez entrer un nom";
	  	elseif (mb_strlen($this->data["nom"], 'UTF-8') <= 3)
			$this->errors["nom"] = "Le nom doit faire moins 5 caractères";
	  
	  	if (!key_exists("prenom", $this->data) || $this->data["prenom"] === "")
			$this->errors["prenom"] = "Vous devez entrer un prenom";
		elseif (!preg_match("/^[A-Za-z]*$/i", $this->data["prenom"]))
			$this->errors["prenom"] = "Caractères autorisés : A....Z et a....z";
		
		if (mb_strlen($this->data["login"], 'UTF-8') <= 3)
			$this->errors["login"] = "Vous devez entrer au minimum 5 caractères ";
    	
		if (mb_strlen($this->data["pass"], 'UTF-8') <= 3)
  			$this->errors["pass"] = "Vous devez entrer exactement 5 caractères ";
    
		if (!preg_match($mailRegex, $this->data["mail"]))
      		$this->errors["mail"] = "Adresse mail invalide";
		
		return count($this->errors) === 0;
	}

	//Cette fonction permet de vérifier les champs du formulaire
	//de connexion d'un utilisateur
	public function isValidConnect() {
		$this->errors = array();

		if (!key_exists("login", $this->data) || $this->data["login"] === "")
			$this->errors["login"] = "Vous devez entrer un login !";
		if (!key_exists("pass", $this->data) || $this->data["pass"] === "")
  			$this->errors["pass"] = "Vous devez entrer un mot de passe ! ";
    	
		return count($this->errors) === 0;
	}

  public function getErrors($ref){
    return key_exists($ref,$this->errors)? $this->errors[$ref]:null;
  }

  public function getData($ref){
    return key_exists($ref, $this->data) ? $this->data[$ref] : '';
  }

  public function getNomRef(){
    return "nom";
  }

  public function getPrenomRef(){
    return "prenom";
  }

  public function getLoginRef(){
    return "login";
  }

  public function getPasswordRef(){
    return "pass";
  }

  public function getMailRef(){
    return "mail";
  }

}

?>

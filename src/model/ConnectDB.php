<?php

set_include_path('./src');
set_include_path(dirname(__FILE__)."/../");
require_once('config/config.php');
require_once('User.php');
class ConnectDB
{
    protected $pdo;
    protected $errors;

    public function __construct()
    {
        try {
            $connect = 'mysql:host=' . HOTE . ';dbname=' . NOM_BASE;
            $this->pdo = new PDO($connect, LOGIN, PASSWORD);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->exec('SET NAMES utf8');
        } catch (PDOException $e) {
            echo $e;
        }
    }

    public function readMusic($id){
      $query= "SELECT * FROM chansons WHERE id_chansons = ?";
      $sth= $this->pdo->prepare($query);
      $sth->execute(array($id));
      $result = $sth->fetchAll(PDO::FETCH_ASSOC);
      return $result;

    }

    public function nbUser() {
        $query = "Select count(id_user) as id from users ";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $rep = $stmt->fetch();
        return $rep['id'];
    }

    public function createUser(User $u) {
            $this->pdo->beginTransaction();
            $query = 'INSERT INTO users (nom,prenom,login,password,mail) VALUES (:nom,:prenom,:login,:password,:mail);';
            $rep = $this->pdo->prepare($query);
            $mdp =  $u->getMpd();
            $passCrypt = password_hash($mdp, PASSWORD_BCRYPT);
            $rep->execute(array(
                ':nom' => $u->getNom(),
                ':prenom' => $u->getPrenom(),
                ':login' =>  $u->getLogin(),
                ':password' => $passCrypt,
                ':mail' => $u->getMail()
            ));
            $this->pdo->commit();
      }

      public function createMusic(array $data) {
              $fichier = $_FILES['photo']['name'];
              $extension = pathinfo($fichier, PATHINFO_EXTENSION);
              $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png', 'bmp','JPG' , 'JPEG' , 'GIF' , 'PNG', 'BMP' );
              if(in_array($extension,$extensions_valides)){
                //echo "hhhhhhh";
                 $artiste = $data['artiste'];
                 $image = "$artiste.$extension";
                 //echo $image;
                  $imgUri ="C:/wamp64/www/projetChanson/src/upload/$artiste.$extension";
                 move_uploaded_file($_FILES['photo']['tmp_name'],$imgUri);
                 $this->pdo->beginTransaction();
                 $query = 'INSERT INTO chansons(artiste,titre,photo,parole,id_user) VALUES(:artiste,:titre,:photo,:parole,:id_user);';
                 $rep = $this->pdo->prepare($query);
                 $donnees = array(
                     ':artiste' => $artiste,
                     ':titre' => $data['titre'],
                     ':photo' => $image,
                     ':parole' => $data['parole'],
                     ':id_user' => $_SESSION['user']['id_user'],
                 );
                 $rep->execute($donnees);
                 //echo "string";
                 //echo $artiste;
                 //var_export($donnees);
                 $this->pdo->commit();
             }else{
               echo "Vous devez charger une image!!!";
             }
        }

    public function connexion(User $u)
    {
      $query = "SELECT * FROM users WHERE login = :login; ";
      $stmt = $this->pdo->prepare($query);
      $donnees = array(":login" => $u->getLogin());
      $stmt->execute($donnees);
      $result = $stmt->fetchAll();
      return $result;
    }

   public function readMyMusic(){

     $query= "SELECT * FROM chansons WHERE id_user = ?";
     $sth= $this->pdo->prepare($query);
     $sth->execute(array($_SESSION['user']['id_user']));
     $result = $sth->fetchAll(PDO::FETCH_ASSOC);
     return $result;

}

/*public function read($id) {
    if (key_exists($id, $this->db))
      return $this->db[$id];
    else
      return null;
  }*/

  public function read($id){

     $query= "SELECT * FROM chansons WHERE id_chanson = ? ";
     $sth= $this->pdo->prepare($query);
     $sth->execute(array($id));
     $result = $sth->fetchAll(PDO::FETCH_ASSOC);
     return $result;

}

   public function readAllMusic(){
     $query = "SELECT * FROM chansons; ";
     $stmt = $this->pdo->prepare($query);
      $stmt->execute();
     $result = $stmt->fetchAll();
     return $result;

   }


   public function update($id, $chanson){
        $fichier = $_FILES['photo']['name'];
        $extension = pathinfo($fichier, PATHINFO_EXTENSION);
        $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png', 'bmp','JPG' , 'JPEG' , 'GIF' , 'PNG', 'BMP' );
       
                 $artiste = $chanson['artiste'];
                 $titre= $chanson['titre'];
                 $parole=$chanson['parole'];
                 $image = "$artiste.$extension";
                 
                 $imgUri = "C:/wamp64/www/projetChanson/src/upload/$artiste.$extension";
                 move_uploaded_file($_FILES['photo']['tmp_name'],$imgUri);
            try{
                 $this->pdo->beginTransaction();
                 $query= "UPDATE chansons SET artiste=?, titre=?, photo=?, parole=? WHERE id_chanson = ?";
                 $sth= $this->pdo->prepare($query);
                 $result =$sth->execute(array($artiste,  $titre, $image, $parole, $id));
                 $this->pdo->commit();
    
                return $result;
             }catch(PDOException $e){
                throw new Exception($e->getMessage());
             } 
    
    }

     public function delete($id){
        try{
            $this->pdo->beginTransaction();
            $query= "DELETE FROM chansons WHERE id_chanson = ?";
            $sth= $this->pdo->prepare($query);
            $result =$sth->execute(array($id));
            $this->pdo->commit();

           return $result;
        }catch(PDOException $e){
           throw new Exception($e->getMessage());
        }  
     }
    }

?>
# ParoleDeChanson
J'ai réalisé ce projet dans le cadre  de l'appronfondissement personnel de mes compétences en PHP Objet et PHP 7.
Passioné de musique, j'ai imaginé un site de partage des paroles de chansons. En effet, Un utilisateur donné peut créer un compte, se connecter, ajouter, les chansons. Il ne peut modifier ou supprimer que des chansons qu'il a ajouté.

